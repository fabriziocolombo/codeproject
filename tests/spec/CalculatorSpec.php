<?php

namespace spec\CodeProject;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CalculatorSpec extends ObjectBehavior
{
    public function itIsInitializable()
    {
        //$this->shouldHaveType('CodeProject\Calculator');
        $this->shouldHaveType('CodeProject\Calculator');
    }

    public function itShouldSum()
    {
        $this->sum(4, 7);
        $this->result()->shouldBe(11);
    }
}
