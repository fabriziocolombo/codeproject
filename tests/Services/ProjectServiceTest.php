<?php

namespace Tests;

use \Illuminate\Database\Eloquent\ModelNotFoundException;
use \Illuminate\Support\MessageBag;
use \Prettus\Validator\Exceptions\ValidatorException;
use \CodeProject\Services\ProjectService;

use Mockery as m;

class ProjectServiceTest extends TestCase
{


    public function setUp()
    {
        parent::setUp();

        $this->valid_data = [
                    "id" => "1",
                    "owner_id" => "2",
                    "client_id" => "7",
                    "name" => "Project B",
                    "description" => "Icie Cummerata",
                    "progress" => "50",
                    "status" => "1",
                    "due_date" => "1987-03-19 15:02:22",
                  ];

        $this->invalid_data = [
                    "id" => "",
                    "owner_id" => "",
                    "client_id" => "",
                    "name" => "",
                    "description" => "",
                    "progress" => "",
                    "status" => "",
                    "due_date" => "",
                  ];

        $this->validator = m::mock('\CodeProject\Validators\ProjectValidator');
        $this->repository = m::mock('\CodeProject\Repositories\ProjectRepository');

        $this->service = new ProjectService($this->repository, $this->validator);

    }

    /**
     * @test
     */
    public function retriveAll()
    {
        $entities = factory(\CodeProject\Entities\Project::class, 2)->make();

        $this->repository->shouldReceive('with->all')->once()->andReturn($entities);

        $expected = [
            'error' => false,
            'data' => $entities
        ];

        $expected = json_encode($expected);

        $actual   = $this->service->all();

        $this->assertEquals(200, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }

    /**
     * @test
     */
    public function findByExistentId()
    {
        $id = 1;
        $entity = factory(\CodeProject\Entities\Project::class)->make(['id' => $id]);

        // Mock ClientRepository->find(id)
        $this->repository->shouldReceive('with->find')
            ->once()
            ->with($id)
            ->andReturn($entity);

        $expected = [
            'error' => false,
            'data' => $entity
        ];

        $expected = json_encode($expected);
        $actual   = $this->service->find($id);

        $this->assertEquals(200, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }

    /**
     * @test
     */
    public function findByNoExistentId()
    {
        $id = 1;
        $error = new ModelNotFoundException;
        $entity = factory(\CodeProject\Entities\Project::class)->make(['id' => 2]);

        $this->repository->shouldReceive('with->find')
            ->once()->with($id)
            ->andThrow($error);

        $expected = [
            'error' => true,
            'message' => 'This Entity does not exist'
        ];

        $expected = json_encode($expected);

        $actual   = $this->service->find($id);

        $this->assertEquals(404, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }

    /**
     * @test
     */
    public function createWithValidData()
    {
        $id = 1;

        $this->validator->shouldReceive('with->passesOrFail')->once();

        $entity =  factory(\CodeProject\Entities\Project::class)->make(['id' => $id ]);

        $this->repository->shouldReceive('create')
            ->once()->with($this->valid_data)
            ->andReturn($entity);

        $expected = [
            'error' => false,
            'message' => 'This Entity has been created',
            'data' => $entity
        ];

        $expected = json_encode($expected);

        $actual   = $this->service->create($this->valid_data);

        $this->assertEquals(201, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }

    /**
     * @test
     */
    public function createWithInvalidData()
    {
        $validations_errors = [
            "owner_id" => ["The owner id field is required."],
            "client_id" => ["The client id field is required."],
            "name" => ["The name field is required."],
            "progress"  => ["The progress field is required."],
            "status"  => ["The status field is required."],
            "due_data"  => ["The due date field is required."]
        ];

        $bag = new \Illuminate\Support\MessageBag($validations_errors);
        $error = new ValidatorException($bag);

        $expected = [
            'error' => true,
            'message' => $bag->getMessageBag()
        ];

        $this->validator->shouldReceive('with->passesOrFail')->once();

        $this->repository->shouldReceive('create')
            ->once()->with($this->invalid_data)
            ->andThrow($error);

        $expected = json_encode($expected);

        $actual   = $this->service->create($this->invalid_data);

        $this->assertEquals(400, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }


    /**
     * @test
     */
    public function updateWithValidData()
    {
        $id = 1;

        $this->validator->shouldReceive('with->passesOrFail');

        $entity =  factory(\CodeProject\Entities\Project::class)->make($this->valid_data);

        // Mock ClientRepository->create($data)
        $this->repository->shouldReceive('update')
            ->once()->with($this->valid_data, $id)
            ->andReturn($entity);

        $expected = [
            'error' => false,
            'message' => 'This Entity has been updated',
            'data' => $entity
        ];

        $expected = json_encode($expected);

        $actual   = $this->service->update($this->valid_data, $id);

        $this->assertEquals(200, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }

    /**
     * @test
     */
    public function deleteExistentId()
    {
        $id = 1;
        $entity = factory(\CodeProject\Entities\Project::class)->make(['id' => $id]);


        $this->repository->shouldReceive('find')
            ->once()
            ->with($id)
            ->andReturn($entity);

        // Mock ClientRepository->find(id)
        $this->repository->shouldReceive('delete')
            ->once()
            ->with($id)
            ->andReturn(true);

        $expected = [
            'error' => false,
            'message' => 'This Entity has been deleted',
            'data' => $entity
        ];

        $expected = json_encode($expected);
        $actual   = $this->service->delete($id);

        $this->assertEquals(200, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }


    /**
     * @test
     */
    public function deleteNoExistentId()
    {
        $id = 1;
        $error = new ModelNotFoundException;
        $entity = factory(\CodeProject\Entities\Project::class)->make(['id' => 2]);

        $this->repository->shouldReceive('with->find')
            ->once()->with($id)
            ->andThrow($error);

        $this->repository->shouldReceive('delete')
            ->never()
            ->with($id)
            ->andReturn(false);

        $expected = [
            'error' => true,
            'message' => 'This Entity does not exist'
        ];

        $expected = json_encode($expected);

        $actual   = $this->service->find($id);

        $this->assertEquals(404, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }

}
