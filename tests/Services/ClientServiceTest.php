<?php

namespace Tests;

use \Illuminate\Database\Eloquent\ModelNotFoundException;
use \Illuminate\Support\MessageBag;
use \Prettus\Validator\Exceptions\ValidatorException;
use \CodeProject\Services\ClientService;

use Mockery as m;

class ClientServiceTest extends TestCase
{


    public function setUp()
    {
        parent::setUp();

        $this->valid_data = [
                    "id" => "1",
                    "name" => "Cliente B",
                    "responsible" => "Icie Cummerata",
                    "email" => "Orin.Ziemann@Becker.info",
                    "phone" => "768.229.9281",
                    "address" => "601 Milford Heights\nGibsonchester, FL 57672",
                    "obs" => "Maxime doloremque quo eum quam reiciendis porro.",
                  ];

        $this->invalid_data = [
                    "id" => "",
                    "name" => "",
                    "responsible" => "",
                    "email" => "",
                    "phone" => "",
                    "address" => "",
                    "obs" => "",
                  ];

        $this->validator = m::mock('\CodeProject\Validators\ClientValidator');
        $this->repository = m::mock('\CodeProject\Repositories\ClientRepository');

        $this->service = new ClientService($this->repository, $this->validator);

    }

    // //use DatabaseMigrations;
    // public function tearDown()
    // {
    //     m::close();
    // }

    /**
     * @test
     */
    public function retriveAll()
    {
        $clients = factory(\CodeProject\Entities\Client::class, 2)->make();
        $this->repository->shouldReceive('all')->once()->andReturn($clients);

        $expected = [
            'error' => false,
            'data' => $clients
        ];

        $expected = json_encode($expected);

        $actual   = $this->service->all();

        $this->assertEquals(200, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }

    /**
     * @test
     */
    public function findByExistentId()
    {
        $param = 1;
        $client = factory(\CodeProject\Entities\Client::class)->make(['id' => $param]);

        // Mock ClientRepository->find(id)
        $this->repository->shouldReceive('find')
            ->once()
            ->with($param)
            ->andReturn($client);

        $expected = [
            'error' => false,
            'data' => $client
        ];

        $expected = json_encode($expected);
        $actual   = $this->service->find($param);

        $this->assertEquals(200, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }

    /**
     * @test
     */
    public function findByNoExistentId()
    {
        $id = 1;
        $error = new ModelNotFoundException;
        $client = factory(\CodeProject\Entities\Client::class)->make(['id' => 2]);

        // Mock ClientRepository->find(id)
        $this->repository->shouldReceive('find')
            ->once()->with($id)
            ->andThrow($error);

        $expected = [
            'error' => true,
            'message' => 'This Entity does not exist'
        ];

        $expected = json_encode($expected);

        $actual   = $this->service->find($id);

        $this->assertEquals(404, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }

    /**
     * @test
     */
    public function createWithValidData()
    {
        $id = 1;

        $this->validator->shouldReceive('with->passesOrFail')->once();

        $client =  factory(\CodeProject\Entities\Client::class)->make(['id' => $id ]);

        // Mock ClientRepository->create($data)
        $this->repository->shouldReceive('create')
            ->once()->with($this->valid_data)
            ->andReturn($client);

        $expected = [
            'error' => false,
            'message' => 'This Entity has been created',
            'data' => $client
        ];

        $expected = json_encode($expected);

        $actual   = $this->service->create($this->valid_data);

        $this->assertEquals(201, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }

    /**
     * @test
     */
    public function createWithInvalidData()
    {
        $validations_errors = [
            "name" => ["The name field is required."],
            "responsible"  => ["The responsible field is required."],
            "email"  => ["The email field is required."],
            "phone"  => ["The phone field is required."],
            "address"  => ["The address field is required."]
        ];

        $bag = new \Illuminate\Support\MessageBag($validations_errors);
        $error = new ValidatorException($bag);

        $expected = [
            'error' => true,
            'message' => $bag->getMessageBag()
        ];

        // Mock ClientValidation->with($data)->passesOrFail()
        $this->validator->shouldReceive('with->passesOrFail')->once();

        // Mock ClientRepository->create($data)
        $this->repository->shouldReceive('create')
            ->once()->with($this->invalid_data)
            ->andThrow($error);

        $expected = json_encode($expected);

        $actual   = $this->service->create($this->invalid_data);

        $this->assertEquals(400, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }


    /**
     * @test
     */
    public function updateWithValidData()
    {
        $id = 1;

        $this->validator->shouldReceive('with->passesOrFail');

        $client =  factory(\CodeProject\Entities\Client::class)->make($this->valid_data);

        // Mock ClientRepository->create($data)
        $this->repository->shouldReceive('update')
            ->once()->with($this->valid_data, $id)
            ->andReturn($client);

        $expected = [
            'error' => false,
            'message' => 'This Entity has been updated',
            'data' => $client
        ];

        $expected = json_encode($expected);

        $actual   = $this->service->update($this->valid_data, $id);

        //  echo "\n"; print_r($expected); echo "\n";
        //  echo "\n"; print_r($actual->getContent()); echo "\n";

        $this->assertEquals(200, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }

    /**
     * @test
     */
    public function deleteExistentId()
    {
        $id = 1;
        $client = factory(\CodeProject\Entities\Client::class)->make(['id' => $id]);


        $this->repository->shouldReceive('find')
            ->once()
            ->with($id)
            ->andReturn($client);

        // Mock ClientRepository->find(id)
        $this->repository->shouldReceive('delete')
            ->once()
            ->with($id)
            ->andReturn(true);

        $expected = [
            'error' => false,
            'message' => 'This Entity has been deleted',
            'data' => $client
        ];

        $expected = json_encode($expected);
        $actual   = $this->service->delete($id);

        $this->assertEquals(200, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }


    /**
     * @test
     */
    public function deleteNoExistentId()
    {
        $id = 1;
        $error = new ModelNotFoundException;
        $client = factory(\CodeProject\Entities\Client::class)->make(['id' => 2]);

        // Mock ClientRepository->find(id)
        $this->repository->shouldReceive('find')
            ->once()->with($id)
            ->andThrow($error);

        $this->repository->shouldReceive('delete')
            ->never()
            ->with($id)
            ->andReturn(false);

        $expected = [
            'error' => true,
            'message' => 'This Entity does not exist'
        ];

        $expected = json_encode($expected);

        $actual   = $this->service->find($id);

        $this->assertEquals(404, $actual->getStatusCode());
        $this->assertEquals($expected, $actual->getContent());
    }

}
