<?php

namespace Tests;

use \Illuminate\Foundation\Testing\WithoutMiddleware;
use \Illuminate\Foundation\Testing\DatabaseMigrations;
use \Illuminate\Foundation\Testing\DatabaseTransactions;

use \Illuminate\Support\Facades\Artisan;

/**
 * @testdox API de Projetos owers status code
 */
class ClientApiStatusCodeTest extends TestCase
{
    use DatabaseMigrations;

    private $valid_data;
    private $invalid_data;

    public function setUp()
    {
        parent::setUp();

        $this->valid_data = [
                    "id" => "1",
                    "name" => "Mr. Emmett McCullough",
                    "responsible" => "Icie Cummerata",
                    "email" => "Orin.Ziemann@Becker.info",
                    "phone" => "768.229.9281",
                    "address" => "601 Milford Heights\nGibsonchester, FL 57672",
                    "obs" => "Maxime doloremque quo eum quam reiciendis porro.",
                  ];

        $this->invalid_data = [
                    "id" => "",
                    "name" => "",
                    "responsible" => "Icie Cummerata",
                    "email" => "Orin.Ziemann@Becker.info",
                    "phone" => "768.229.9281",
                    "address" => "601 Milford Heights\nGibsonchester, FL 57672",
                    "obs" => "Maxime doloremque quo eum quam reiciendis porro.",
                  ];

                //  $this->seeds();
    }

    public function seeds()
    {
          factory(\CodeProject\Entities\Client::class)->create([
                  'id' => 1,
                  'name' => 'Client A',
                ]);
    }

    /**
     * @test
     */
    public function responseWithCode200OkWhenSendAGetRequestWithoutId()
    {
        $this->seeds();
        $response = $this->call('GET', '/client');
        $this->assertEquals(200, $response->status());
    }

    /**
     * @test find with valid id
     */
    public function responseWithCode200OkWhenSendAGetRequestWithValidId()
    {
        $this->seeds();
        $response = $this->call('GET', '/client/1');
        $this->assertEquals(200, $response->status());
    }

    /**
     * find with invalid id
     * @test
     */
    public function responseWithCode404NotFoundWhenSendAGetRequestWithInvalidId()
    {
        $response = $this->call('GET', '/client/100');
        $this->assertEquals(404, $response->status());
    }

    /**
     * @test create with valid data
     */
    public function responseWithCode201CreatedWhenSendAPostRequestWithValidData()
    {
        $response = $this->call('POST', '/client', $this->valid_data);
        $this->assertEquals(201, $response->status());
    }

    // create with ivalid data
    /**
     * @test create with invalid data
     */
    public function responseWithCode400BadRequestWhenSendAPostRequestWithIvalidData()
    {
        $response = $this->call('POST', '/client', $this->invalid_data);
        $this->assertEquals(400, $response->status());
    }

    /**
     * @test update a valid project with valid data
     */
    public function responseWithCode200OkWhenSendAPutRequestWithValidIdAndValidData()
    {
        $this->seeds();
        $response = $this->call('PUT', '/client/1', $this->valid_data);
        $this->assertEquals(200, $response->status());
    }

    /**
     * @test update a valid project with invalid data
     */
    public function responseWithCode400BadRequestWhenSendAPutRequestWithValidIdAndInvalidData()
    {
        $response = $this->call('PUT', '/client/1', $this->invalid_data);
        $this->assertEquals(400, $response->status());
    }

    /**
     * @test update a invalid project
     */
    public function responseWithCode404NotFoundWhenSendAPutRequestWithInvalidId()
    {
        $response = $this->call('PUT', '/client/100', $this->valid_data);
        $this->assertEquals(404, $response->status());
    }

    /**
     * @test delete a invalid project
     */
    public function responseWithCode404NotFoundWhenSendADeleteRequestWithInvalidId()
    {
        $response = $this->call('DELETE', '/client/100');
        $this->assertEquals(404, $response->status());
    }

    /**
     * @test delete a valid project
     */
    public function responseWithCode200OkWhenSendADeleteRequestWithValidId()
    {
        $this->seeds();
        $response = $this->call('DELETE', '/client/1');
        $this->assertEquals(200, $response->status());
    }
}
