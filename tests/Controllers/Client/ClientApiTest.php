<?php

namespace Tests;

use \Illuminate\Foundation\Testing\WithoutMiddleware;
use \Illuminate\Foundation\Testing\DatabaseMigrations;
use \Illuminate\Foundation\Testing\DatabaseTransactions;

use \Illuminate\Support\Facades\Artisan;

class ClientApiTest extends TestCase
{
    use DatabaseMigrations;

    private $valid_data;
    private $invalid_data;

    public function setUp()
    {
        parent::setUp();

        $this->valid_data = [
                    "id" => "1",
                    "name" => "Cliente B",
                    "responsible" => "Icie Cummerata",
                    "email" => "Orin.Ziemann@Becker.info",
                    "phone" => "768.229.9281",
                    "address" => "601 Milford Heights\nGibsonchester, FL 57672",
                    "obs" => "Maxime doloremque quo eum quam reiciendis porro.",
                  ];

        $this->invalid_data = [
                    "id" => "",
                    "name" => "",
                    "responsible" => "",
                    "email" => "",
                    "phone" => "",
                    "address" => "",
                    "obs" => "",
                  ];
    }

    public function seeds()
    {
        factory(\CodeProject\Entities\Client::class)->create([
              'id' => 1,
              'name' => 'Cliente A',
            ]);
    }

    /**
     * @test
     */
    public function listAll()
    {
        $this->seeds();
        $this->get('/client')
            ->seeJson(['error' => false, 'name' => 'Cliente A']);
    }

    /**
     * @test
     */
    public function findByExistent()
    {
        $this->seeds();
        $this->get('/client/1')
            ->seeJson(['error' => false, 'name' => 'Cliente A']);
    }

    /**
     * @test
     */
    public function findByNoExistent()
    {
        $this->get('/client/100')
            ->seeJson(['error' => true,  'message' => 'This Entity does not exist']);
    }


    /**
     * @test
     */
    public function createWithValidData()
    {
        $this->post('/client', $this->valid_data)
            ->seeJson(['error' => false,  'name' => 'Cliente B']);
    }

    /**
     * @test
     */
    public function createWithInvalidData()
    {
        $this->post('/client', $this->invalid_data)
              ->seeJson([
                  'error' => true,
                  'name' => ["The name field is required."],
                  'address' => ["The address field is required."],
                  'email' => ["The email field is required."],
                  'phone' => ["The phone field is required."],
                  'responsible' => ["The responsible field is required."],
              ]);
    }


    /**
     * @test
     */
    public function updateAnExistentWithValidData()
    {
        $this->seeds();
        $this->put('/client/1', $this->valid_data)
              ->seeJson(['error' => false,  'name' => 'Cliente B']);
    }

    /**
     * @test
     */
    public function updateAnExistentWithInvalidData()
    {
        $this->put('/client/1', $this->invalid_data)
                ->seeJson([
                  'error' => true,
                  'name' => ["The name field is required."],
                  'address' => ["The address field is required."],
                  'email' => ["The email field is required."],
                  'phone' => ["The phone field is required."],
                  'responsible' => ["The responsible field is required."],
                  ]);
    }

    /**
     * @test
     */
    public function updateAnNoExistent()
    {
        $this->put('/client/100', $this->valid_data)
                ->seeJson([
                  'error' => true,
                  'message' => 'This Entity does not exist'
                ]);
    }

    /**
     * @test
     */
    public function deleteAnNoExistent()
    {
        $this->delete('/client/100')
                ->seeJson([
                  'error' => true,
                  'message' => 'This Entity does not exist',
                ]);
    }

    /**
     * @test
     */
    public function deleteAnExistent()
    {
        $this->seeds();
        $this->delete('/client/1')
              ->seeJson([
                  'error' => false,
                  'message' => 'This Entity has been deleted'
                ]);
    }
}
