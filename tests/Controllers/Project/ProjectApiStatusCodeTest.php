<?php

namespace Tests;

use \Illuminate\Foundation\Testing\WithoutMiddleware;
use \Illuminate\Foundation\Testing\DatabaseMigrations;
use \Illuminate\Foundation\Testing\DatabaseTransactions;

use \Illuminate\Support\Facades\Artisan;

/**
 * @testdox API de Projetos owers status code
 */
class ProjectApiStatusCodeTest extends TestCase
{
    use DatabaseMigrations;

    private $valid_data;
    private $invalid_data;

    public function setUp()
    {
        parent::setUp();

        $this->valid_data = [
                      "owner_id" => "1",
                      "client_id" => "1",
                      "name"=> "Projeto B",
                      "description" => "Sequi distinctio quia ducimus itaque sed labore commodi dolorem.",
                      "progress" => "19",
                      "status"=>  "1",
                      "due_date" => "1984-07-02 05:14:05"
                  ];
        $this->invalid_data = [
                      "owner_id" => "",
                      "client_id" => "",
                      "name"=> "",
                      "description" => "",
                      "progress" => "",
                      "status"=>  "",
                  ];

                //  $this->seeds();
    }

    public function seeds()
    {
        factory(\CodeProject\Entities\Client::class)->create([
              'id' => 1,
            ]);
        factory(\CodeProject\Entities\Project::class)->create([
              'id'=> 1,
              'owner_id' => 1,
              'client_id' => 1,
              'name' => 'Projeto A'
            ]);
    }

    /**
     * @test
     */
    public function responseWithCode200OkWhenSendAGetRequestWithoutId()
    {
        $this->seeds();
        $response = $this->call('GET', '/project');
        $this->assertEquals(200, $response->status());
    }

    /**
     * @test find with valid id
     */
    public function responseWithCode200OkWhenSendAGetRequestWithValidId()
    {
        $this->seeds();
        $response = $this->call('GET', '/project/1');
        $this->assertEquals(200, $response->status());
    }

    /**
     * find with invalid id
     * @test
     */
    public function responseWithCode404NotFoundWhenSendAGetRequestWithInvalidId()
    {
        $response = $this->call('GET', '/project/100');
        $this->assertEquals(404, $response->status());
    }

    /**
     * @test create with valid data
     */
    public function responseWithCode201CreatedWhenSendAPostRequestWithValidData()
    {
        //$this->seeds();
        $response = $this->call('POST', '/project', $this->valid_data);
        $this->assertEquals(201, $response->status());
    }

    /**
     * @test create with invalid data
     */
    public function responseWithCode400BadRequestWhenSendAPostRequestWithIvalidData()
    {
        //$this->seeds();
        $response = $this->call('POST', '/project', $this->invalid_data);
        $this->assertEquals(400, $response->status());
    }

    /**
     * @test update a valid project with valid data
     */
    public function responseWithCode200OkWhenSendAPutRequestWithValidIdAndValidData()
    {
        $this->seeds();
        $response = $this->call('PUT', '/project/1', $this->valid_data);
        $this->assertEquals(200, $response->status());
    }

    /**
     * @test update a valid project with invalid data
     */
    public function responseWithCode400BadRequestWhenSendAPutRequestWithValidIdAndInvalidData()
    {
        $response = $this->call('PUT', '/project/1', $this->invalid_data);
        $this->assertEquals(400, $response->status());
    }

    /**
     * @test update a invalid project
     */
    public function responseWithCode404NotFoundWhenSendAPutRequestWithInvalidId()
    {
        $response = $this->call('PUT', '/project/100', $this->valid_data);
        $this->assertEquals(404, $response->status());
    }

    /**
     * @test delete a invalid project
     */
    public function responseWithCode404NotFoundWhenSendADeleteRequestWithInvalidId()
    {
        $response = $this->call('DELETE', '/project/100');
        $this->assertEquals(404, $response->status());
    }

    /**
     * @test delete a valid project
     */
    public function responseWithCode200OkWhenSendADeleteRequestWithValidId()
    {
        $this->seeds();
        $response = $this->call('DELETE', '/project/1');
        $this->assertEquals(200, $response->status());
    }
}
