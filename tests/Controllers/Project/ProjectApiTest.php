<?php

namespace Tests;

use \Illuminate\Foundation\Testing\WithoutMiddleware;
use \Illuminate\Foundation\Testing\DatabaseMigrations;
use \Illuminate\Foundation\Testing\DatabaseTransactions;

use \Illuminate\Support\Facades\Artisan;

class ProjectApiTest extends TestCase
{
    use DatabaseMigrations;

    private $valid_data;
    private $invalid_data;

    public static $setupDatabase = true;

    // public function setupDatabase()
    // {
    //     echo "\n---initDB---\n";
    //     Artisan::call('migrate:refresh');
    //     Artisan::call('db:seed');
    //     self::$setupDatabase = false;
    //
    // }


    public function setUp()
    {
        parent::setUp();
        //  if(self::$setupDatabase)
        //   {
        //       $this->setupDatabase();
        //   }


        $this->valid_data = [
                      "owner_id" => "1",
                      "client_id" => "1",
                      "name"=> "Projeto B",
                      "description" => "Sequi distinctio quia ducimus itaque sed labore commodi dolorem.",
                      "progress" => "19",
                      "status"=>  "1",
                      "due_date" => "1984-07-02 05:14:05"
                  ];
        $this->invalid_data = [
                      "owner_id" => "",
                      "client_id" => "",
                      "name"=> "",
                      "description" => "",
                      "progress" => "",
                      "status"=>  "",
                  ];
    }

    public function seeds()
    {
        factory(\CodeProject\Entities\Client::class)->create([
              'id' => 1,
            ]);
        factory(\CodeProject\Entities\Project::class)->create([
              'id'=> 1,
              'owner_id' => 1,
              'client_id' => 1,
              'name' => 'Projeto A'
            ]);
    }

    /**
     * @test
     */
    public function listAll()
    {
        $this->seeds();
        $this->get('/project')
            ->seeJson([
               'error' => false,
               'name' => 'Projeto A'
            ]);
    }

    /**
     * @test
     */
    public function findByExistent()
    {
        $this->seeds();
        $this->get('/project/1')
            ->seeJson([
                'error' => false,
                'name' => 'Projeto A'
            ]);
    }

    /**
     * @test
     */
    public function findByNoExistent()
    {
        $this->get('/project/100')
            ->seeJson([
               'error' => true,
               'message' => 'This Entity does not exist'
            ]);
    }

    /**
     * @test
     */
    public function createWithValidData()
    {
        $this->post('/project', $this->valid_data)
              ->seeJson([
                  'error' => false,
                  'name' => 'Projeto B'
                ]);
    }

    /**
     * @test
     */
    public function createWithInvalidData()
    {
        $this->post('/project', $this->invalid_data)
              ->seeJson([
                  'error' => true,
                  'owner_id' => ["The owner id field is required."],
                  'client_id' => ["The client id field is required."],
                  'progress' => ["The progress field is required."],
                  'status' => ["The status field is required."],
              ]);
    }

    /**
     * @test
     */
    public function updateAnExistentWithValidData()
    {
        $this->seeds();
        $this->put('/project/1', $this->valid_data)
                ->seeJson([
                    'error' => false,
                    'name' => 'Projeto B'
                ]);
    }

    /**
     * @test
     */
    public function updateAnExistentWithInvalidData()
    {
        $this->put('/project/1', $this->invalid_data)
              ->seeJson([
                  'error' => true,
                  'owner_id' => ["The owner id field is required."],
                  'client_id' => ["The client id field is required."],
                  'progress' => ["The progress field is required."],
                  'status' => ["The status field is required."],
              ]);
    }

    /**
     * @test
     */
    public function updateAnNoExistent()
    {
        $this->put('/project/100', $this->valid_data)
            ->seeJson([
                'error' => true,
                'message' => 'This Entity does not exist'
            ]);
    }

    /**
     * @test
     */
    public function deleteAnNoExistent()
    {
        $this->delete('/project/100')
            ->seeJson([
              'error' => true,
              'message' => 'This Entity does not exist',
            ]);
    }

    /**
     * @test
     */
    public function deleteAnExistent()
    {
        $this->seeds();
        $this->delete('/project/1')
            ->seeJson([
                  'error' => false,
                  'message' => 'The Entity has been deleted'
            ]);
    }
}
