<?php

namespace CodeProject\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\ResponseFactory;

use Acme\Notifications\DomainNotification;

class ResponseApiServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @param  ResponseFactory  $factory
     * @return void
     */
    public function boot(ResponseFactory $factory, DomainNotification $notifications)
    {
        $factory->macro('api', function ($data, $code = 200) use ($factory, $notifications) {

            $datas = array_merge(['error' => false], $data);

            if ($notifications->notify()) {
                 return response()->json(['error' => true, 'message' => $notifications->notify()], 400);
            }

            return response()->json($datas, $code);
            //return $factory->make(strtoupper($value));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
