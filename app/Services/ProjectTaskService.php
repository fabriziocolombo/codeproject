<?php

namespace CodeProject\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Collection as CollectionEloquente;

use CodeProject\Repositories\ProjectTaskRepository;
use CodeProject\Validators\ProjectTaskValidator;
use CodeProject\Transformers\ProjectTaskTransformer;

use Prettus\Validator\Exceptions\ValidatorException;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

use Acme\Notifications\DomainNotification;

class ProjectTaskService
{

    protected $repository;
    protected $validator;

    public function __construct(
        ProjectTaskRepository $repository,
        ProjectTaskValidator $validator,
        Manager $fractal,
        ProjectTaskTransformer $transformer,
        DomainNotification $notifications
    ) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->fractal  = $fractal;
        $this->transformer  = $transformer;
        $this->notifications = $notifications;
    }

    public function all($project_id)
    {
        $entities = $this->repository->findWhere(['project_id' => $project_id ]);
        if ($entities->isEmpty()) {
            $this->notifications->add('This Entity does not exist');
        }

        $entities = $this->transform($entities);

        return $entities;
    }

    public function find($project_id, $task_id)
    {
        $entity = $this->repository->findWhere(['project_id' => $project_id, 'id' => $task_id]);
        if ($entity->isEmpty()) {
            $this->notifications->add('This Entity does not exist');
        }

        $entity = $this->transform($entity);

        return $entity;
    }

    public function delete($id)
    {
        try {
            $entity = $this->repository->delete($id);
            return $entity;

        } catch (ModelNotFoundException $e) {
            $this->notifications->add('This Entity does not exist');
            return false;
        }
    }

    public function create($data)
    {
        if (! $this->validate($data)) {
            return false;
        }

        $entity = $this->repository->create($data);
        $entity = $this->transform($entity);

        return $entity;
    }

    public function update($data, $id)
    {
        if (! $this->validate($data)) {
            return false;
        }

        try {
            $entity = $this->repository->update($data, $id);
            $entity = $this->transform($entity);

            return $entity;

        } catch (ModelNotFoundException $e) {
            $this->notifications->add('This Entity does not exist');
            return false;
        }
    }








        /**
         * Metodo Helper
         * @TODO: extrair metodos
         */
    private function transform($entity)
    {

        if ($entity instanceof CollectionEloquente) {
            $entity = new Collection($entity, $this->transformer);
        } else {
            $entity = new Item($entity, $this->transformer);
        }

            $entity = $this->fractal->createData($entity)->toArray();
            return $entity['data'];
    }

    private function validate(array $data)
    {
        if (! $this->validator->with($data)->passes()) {
            $this->notifications->add($this->validator->errors());
            return false;
        }

        return true;

    }
}
