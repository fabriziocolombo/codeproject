<?php

namespace CodeProject\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Collection as CollectionEloquente;
use Illuminate\Contracts\Filesystem\Factory as Storage;
use Illuminate\FileSystem\Filesystem;

use Prettus\Validator\Exceptions\ValidatorException;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

use CodeProject\Repositories\ProjectRepository;
use CodeProject\Repositories\ProjectFileRepository;
use CodeProject\Transformers\ProjectTransformer;
use CodeProject\Transformers\MemberTransformer;
use CodeProject\Validators\ProjectValidator;


use Acme\Notifications\DomainNotification;

class ProjectService
{

    protected $repository;
    protected $validator;
    protected $fractal;
    protected $transformer;
    protected $member_transformer;
    protected $filesystem;
    protected $storage;
    protected $notifications;

    public function __construct(
        ProjectRepository $repository,
        ProjectValidator $validator,
        Manager $fractal,
        ProjectTransformer $project_transformer,
        MemberTransformer $member_transformer,
        Filesystem $filesystem,
        Storage $storage,
        DomainNotification $notifications
    ) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->fractal  = $fractal;
        $this->transformer  = $project_transformer;
        $this->member_transformer  = $member_transformer;
        $this->filesystem  = $filesystem;
        $this->storage = $storage;

        $this->notifications = $notifications;
    }

    public function allByOwner($owner_id)
    {
        $entities = $this->repository->with(['owner', 'client'])->findWhere(['owner_id' => $owner_id ]);
        if ($entities->isEmpty()) {
            $this->notifications->add('This Entity does not exist');
        }

        $entities = $this->transform($entities, $this->transformer);

        return $entities;
    }


    public function find($id)
    {
            $entity = $this->repository->with(['owner', 'client'])->findWhere(['id' => $id]);
        if ($entity->isEmpty()) {
            $this->notifications->add('This Entity does not exist');
        }

            $entity = $this->transform($entity, $this->transformer);

            return $entity;
    }

    public function delete($id)
    {
        try {
            $entity = $this->repository->delete($id);
            return $entity;

        } catch (ModelNotFoundException $e) {
            $this->notifications->add('This Entity does not exist');
            return false;
        }
    }

    public function create($data)
    {
        if (! $this->validate($data)) {
            return false;
        }

        $entity = $this->repository->create($data);
        $entity = $this->transform($entity, $this->transformer);

        return $entity;
    }

    public function update($data, $id)
    {
        if (! $this->validate($data)) {
            return false;
        }

        try {
            $entity = $this->repository->update($data, $id);
            $entity = $this->transform($entity, $this->transformer);

            return $entity;

        } catch (ModelNotFoundException $e) {
            $this->notifications->add('This Entity does not exist');
            return false;
        }

    }





    /**
     * Metodo para manipular memberships
     * @TODO: extrair metodos
     */
    public function allByProject($project_id)
    {
        $entities = $this->repository->find($project_id);
        $members = $entities->members;
        if ($members->isEmpty()) {
            $this->notifications->add('This Project does not have members');
             return false;
        }

        $entities = $this->transform($members, $this->member_transformer);

        return $entities;
    }

    /**
     * Metodo para manipular memberships
     * @TODO: extrair metodos
     */
    public function addMembership($membership, $project_id)
    {
        try {
            $member = $membership['user_id'];
            $project = $this->repository->find($project_id);

            if ($project->members->contains($member)) {
                throw new ModelNotFoundException("This member is already associated with this project", 1);
            }


            $project->members()->attach($member);
            $member_associated = $project->members()->find($member);
            return response()->json([
                    'error' => false,
                    'message' => 'This member has been added',
                    'data' => $member_associated
                ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => true, 'message' => $e->getMessage()], 404);

        }
    }

    /**
     * Metodo para manipular memberships
     * @TODO: extrair metodos
     */
    public function removeMembership($project_id, $member_id)
    {

        try {
            $project = $this->repository->find($project_id);

            if (!$project->members->contains($member_id)) {
                throw new ModelNotFoundException("This member is not associated with this project", 1);
            }

            $member_dissociated = $project->members()->find($member_id);

            $project->members()->detach($member_id);
            return response()->json([
                   'error' => false,
                   'message' => 'This member has been dissociated',
                   'data' => $member_dissociated
               ], 200);

        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => true, 'message' => $e->getMessage()], 404);
        }
    }

    /**
     * Metodo para manipular memberships
     * @TODO: extrair metodos
     */
    public function isMember($project_id, $member_id)
    {
            $project = $this->repository->find($project_id);
            $member = $project->members()->find($member_id);

        if (! $member) {
            $this->notifications->add('This member is not associated with this project');
            return false;
        }
            $entity = $this->transform($member, $this->member_transformer);

            return $entity;
    }



    /**
     * Metodo para manipular files
     * @TODO: extrair metodos
     */
    public function createFile(array $data)
    {
        $project = $this->repository->find($data['project_id']);
        $new_file = $project->files()->create($data);

        $this->storage->put($new_file->filename, $this->filesystem->get($data['file']));

        if (! $this->storage->exists($new_file->filename)) {
            return false;
        }

        return $new_file;
    }

    /**
     * Metodo para manipular files
     * @TODO: extrair metodos
     */
    public function deleteFile($project_id, $file_id)
    {
            $project = $this->repository->find($project_id);
            $file = $project->files()->find($file_id);

        if (! $file) {
            return false;
        }

        if ($this->storage->delete($file->filename)) {
            $file->delete();
            return $file;
        }

            return false;
    }


    /**
     * Metodo Helper
     * @TODO: extrair metodos
     */
    private function transform($entity, $transformer)
    {

        if ($entity instanceof CollectionEloquente) {
            $entity = new Collection($entity, $transformer);
        } else {
            $entity = new Item($entity, $transformer);
        }

        $entity = $this->fractal->createData($entity)->toArray();
        return $entity['data'];
    }

    private function validate(array $data)
    {
        if (! $this->validator->with($data)->passes()) {
            $this->notifications->add($this->validator->errors());
            return false;
        }

        return true;

    }
}
