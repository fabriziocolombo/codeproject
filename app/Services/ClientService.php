<?php

namespace CodeProject\Services;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Collection as CollectionEloquente;

use Prettus\Validator\Exceptions\ValidatorException;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

use CodeProject\Repositories\ClientRepository;
use CodeProject\Validators\ClientValidator;
use CodeProject\Transformers\ClientTransformer;

use Acme\Notifications\DomainNotification;

class ClientService
{

    protected $repository;
    protected $validator;
    protected $fractal;
    protected $transformer;
    protected $notifications;

    public function __construct(
        ClientRepository $repository,
        ClientValidator $validator,
        Manager $fractal,
        ClientTransformer $transformer,
        DomainNotification $notifications
    ) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->fractal  = $fractal;
        $this->transformer  = $transformer;

        $this->notifications = $notifications;
    }

    public function all()
    {
        $entities = $this->repository->all();
        if ($entities->isEmpty()) {
            $this->notifications->add('This Entity does not exist');
        }

        //$entities = $this->transform($entities, $this->transformer);

        return $entities;
    }

    public function find($id)
    {
        $entity = $this->repository->findWhere(['id' => $id]);
        if ($entity->isEmpty()) {
            $this->notifications->add('This Entity does not exist');
        }

        $entity = $this->transform($entity);

        return $entity;
    }

    public function delete($id)
    {
        try {
            $entity = $this->repository->delete($id);
            return $entity;

        } catch (ModelNotFoundException $e) {
            $this->notifications->add('This Entity does not exist');
            return false;
        }
    }

    public function create($data)
    {
        try {
            $this->validator->with($data)->passesOrFail();
            $entity = $this->repository->create($data);
            return response()->json([
                    'error' => false,
                    'message' => 'This Entity has been created',
                    'data' => $entity
                ], 201);
        } catch (ValidatorException $e) {
            return response()->json(['error' => true, 'message' => $e->getMessageBag()], 400);
        }
    }

    public function update($data, $id)
    {
        if (! $this->validate($data)) {
            return false;
        }

        try {
            $entity = $this->repository->update($data, $id);
            //$entity = $this->transform($entity);

            return $entity;

        } catch (ModelNotFoundException $e) {
            $this->notifications->add('This Entity does not exist');
            return false;
        }
    }






    /**
     * Metodo Helper
     * @TODO: extrair metodos
     */
    private function transform($entity)
    {

        if ($entity instanceof CollectionEloquente) {
            $entity = new Collection($entity, $this->transformer);
        } else {
            $entity = new Item($entity, $this->transformer);
        }

        $entity = $this->fractal->createData($entity)->toArray();
        return $entity['data'];
    }

    private function validate(array $data)
    {
        if (! $this->validator->with($data)->passes()) {
            $this->notifications->add($this->validator->errors());
            return false;
        }

        return true;

    }
}
