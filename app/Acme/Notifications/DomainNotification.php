<?php

namespace Acme\Notifications;

use Illuminate\Session\Store;

// trait Singleton
// {
//     protected static $instance;
//     final public static function instance()
//     {
//         return isset(static::$instance)
//             ? static::$instance
//             : static::$instance = new static;
//     }
//     final private function __construct()
//     {
//         $this->init();
//     }
//     protected function init()
//     {
//     }
//     final private function __wakeup()
//     {
//     }
//     final private function __clone()
//     {
//     }
// }



class DomainNotification
{
    //use Singleton;
    private $notifications = [];

    public function add($notification)
    {
        if (is_array($notification)) {
            $this->addNotifications($notification);
        } else {
            $this->notifications[] = $notification;
        }
    }

    public function addNotifications(array $messages)
    {
        foreach ($messages as $message) {
            $this->notifications[] = $message;
        }
    }

    public function hasNotifications()
    {
        return !empty($this->notifications);
    }

    public function notify()
    {
        return  $this->notifications;
    }

}
