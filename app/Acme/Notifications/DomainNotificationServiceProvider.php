<?php

namespace Acme\Notifications;

use Illuminate\Support\ServiceProvider;

class DomainNotificationServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function register()
    {
        $this->app->singleton('Acme\Notifications\DomainNotification');
    }
}
