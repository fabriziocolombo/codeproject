<?php

namespace CodeProject\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use CodeProject\Repositories\ProjectRepository;
use CodeProject\Entities\Project;

class ProjectRepositoryEloquent extends BaseRepository implements ProjectRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Project::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * @TODO: extrair metodos
     */
    public function isOwner($project_id, $owner_id)
    {
        if (count($this->findWhere(['id' => $project_id, 'owner_id'=> $owner_id]))) {
            return true;
        }
        return false;
    }

    /**
     * @TODO: extrair metodos
     */
    public function hasMember($project_id, $member_id)
    {
        $project = $this->find($project_id);

        $project->members->each(function ($member, $key) use ($member_id) {
            if ($member->id == $member_id) {
                 return true;
            }
        });

        return false;
    }
}
