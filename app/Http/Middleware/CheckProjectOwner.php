<?php

namespace CodeProject\Http\Middleware;

use Closure;

use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use CodeProject\Repositories\ProjectRepository;

class CheckProjectOwner
{
    private $repository;

    public function __construct(ProjectRepository $repository)
    {
        $this->repository = $repository;
        $this->owner_id = Authorizer::getResourceOwnerId();
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->repository->isOwner($request->project, $this->owner_id)) {
            return response('Unauthorized Owner.', 401);
        }

        return $next($request);
    }
}
