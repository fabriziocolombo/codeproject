<?php

namespace CodeProject\Http\Middleware;

use Closure;

use LucaDegasperi\OAuth2Server\Facades\Authorizer;
use CodeProject\Repositories\ProjectRepository;

class CheckProjectPermissions
{
    private $repository;

    public function __construct(ProjectRepository $repository)
    {
        $this->repository = $repository;
        $this->owner_id = Authorizer::getResourceOwnerId();
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->checkProjectPermissions($request->project) == false) {
            return response('Unauthorized Permissions for this project.', 401);
        }

        return $next($request);
    }


    private function checkProjectOwner($project_id)
    {
        $user_id =  $this->owner_id;
        return $this->repository->isOwner($project_id, $user_id);
    }

    private function checkProjectMember($project_id)
    {
        return $this->repository->hasMember($project_id, $this->owner_id);
    }

    private function checkProjectPermissions($project_id)
    {
        if ($this->checkProjectMember($project_id) or $this->checkProjectOwner($project_id)) {
            return true;
        }

        return false;
    }
}
