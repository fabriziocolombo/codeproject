<?php

Route::get('/', function () {
    return view('app');
});

//Route::get('project/{id}/notes', 'ProjectNoteController@index');
//Route::post('project/{id}/notes', 'ProjectNoteController@store');

//Route::get('project/{id}/notes/{noteId}', 'ProjectNoteController@show');
//Route::put('project/{id}/notes/{noteId}', 'ProjectNoteController@update');
//Route::delete('project/{id}/notes/{noteId}', 'ProjectNoteController@destroy');

//Route::get('client', ['middleware' => 'oauth', 'as'=>'client.index', 'uses' => 'ClientController@index']);


Route::group(['middleware' => 'oauth'], function () {

    // Route::group(['prefix'=>'project'], function() {
    //         //Route::resource('project', 'ProjectController', ['except'=>['create', 'edit']]);
    //     Route::get('{id}/note', 'ProjectNoteController@index');
    //     Route::get('{id}/note/{noteId}', 'ProjectNoteController@show');
    //     Route::post('{id}/note', 'ProjectNoteController@store');
    //     Route::put('{id}/note/{noteId}', 'ProjectNoteController@update');
    //     Route::delete('{id}/note/{noteId}', 'ProjectNoteController@destroy');
    // });
    // Route::group(['middleware'=>'check-project-owner'], function(){
    //     Route::resource('project', 'ProjectController', ['except' => ['create', 'edit']]);
    // });

    Route::resource('client', 'ClientController', ['except' => ['create', 'edit']]);
    Route::resource('project', 'ProjectController', ['except' => ['create', 'edit']]);
    Route::resource('project.note', 'ProjectNoteController', ['except' => ['create', 'edit']]);
    Route::resource('project.task', 'ProjectTaskController');
    //Route::resource('project.memberships', 'ProjectMemberController');
    Route::resource('project.members', 'ProjectMemberController');
    Route::resource('project.files', 'ProjectFileController');
});

Route::post('oauth/access_token', function () {
    return Response::json(Authorizer::issueAccessToken());
});
