<?php

namespace CodeProject\Http\Controllers;

use Illuminate\Http\Request;
use CodeProject\Repositories\ClientRepository;
use CodeProject\Services\ClientService;

class ClientController extends Controller
{
    protected $repository;
    protected $service;

    public function __construct(ClientRepository $repository, ClientService $service)
    {
        $this->repository = $repository;
        $this->service = $service;

        //$this->middleware('check-project-permissions', ['only' => ['index', 'show']]);
        //$this->middleware('check-project-owner', ['only' => ['store','update','destroy']]);
    }

    public function index()
    {
        $data = $this->service->all();
        return response()->api(['data' => $data]);
    }

    public function show($id)
    {
        $data = $this->service->find($id);
        return response()->api(['data' => $data]);
    }

    public function store(Request $request)
    {
        $data = $this->service->create($request->all());

        return response()->api(['data' => $data,
                                'message' => 'This Entity has been created' ]);
    }


    public function update(Request $request, $id)
    {
         $data = $this->service->update($request->all(), $id);

         return response()->api(['data' => $data,
                                 'message' => 'This Entity has been updated' ]);
    }

    public function destroy($id)
    {
        $data = $this->service->delete($id);
        return response()->api(['data' => $data,
                                'message' => 'This Entity has been deleted' ]);
    }
}
