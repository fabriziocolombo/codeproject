<?php

namespace CodeProject\Http\Controllers;

use Illuminate\Http\Request;

use CodeProject\Services\ProjectService;

class ProjectController extends ApiController
{
    protected $service;
    protected $notifications;

    public function __construct(ProjectService $service)
    {
        $this->service = $service;
        parent::__construct();

        //$this->middleware('check-project-permissions', ['only' => ['show']]);
        $this->middleware('check-project-owner', ['only' => ['update','destroy']]);
    }

    public function index()
    {
        $data = $this->service->allByOwner($this->owner_id);

        return response()->api(['data' => $data]);
    }

    public function show($id)
    {
        $data = $this->service->find($id);

        return response()->api(['data' => $data]);
    }

    public function store(Request $request)
    {
        $data = $this->service->create($request->all());

        return response()->api(['data' => $data,
                                'message' => 'This Entity has been created' ]);
    }

    public function update(Request $request, $id)
    {
         $data = $this->service->update($request->all(), $id);

         return response()->api(['data' => $data,
                                 'message' => 'This Entity has been updated' ]);
    }

    public function destroy($id)
    {
        $data = $this->service->delete($id);

        return response()->api(['data' => $data,
                                'message' => 'This Entity has been deleted' ]);
    }
}
