<?php

namespace CodeProject\Http\Controllers;

use Storage;
use File;

use Illuminate\Http\Request;
use CodeProject\Repositories\ProjectRepository;
use CodeProject\Services\ProjectService;
use CodeProject\Http\Requests\CreateFileRequest;

use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class ProjectFileController extends Controller
{
    protected $repository;
    protected $service;

    public function __construct(ProjectRepository $repository, ProjectService $service)
    {
        $this->repository = $repository;
        $this->service = $service;

        $this->middleware('check-project-permissions', ['only' => ['index', 'show']]);
        $this->middleware('check-project-owner', ['only' => ['store','update','destroy']]);
    }

    public function store(CreateFileRequest $request, $project_id)
    {

        //if (! $request->hasFile('file')) return 'false';

        $file = $request->file('file');
        $extension = $file->guessExtension();

        $data['file'] = $file;
        $data['extension'] = $extension;
        $data['name'] = $request->name;
        $data['project_id'] = $project_id;
        $data['description'] = $request->description;

        if ($file_created = $this->service->createFile($data)) {
            return response()->json(['error' => false,
            'message' => "The file {$file_created->name} was created with id {$file_created->id}",
            'data' => $file_created ], 200);
        }
    }

    public function destroy($project_id, $file_id)
    {

        if ($file_deleted = $this->service->deleteFile($project_id, $file_id)) {
            return response()->json(['error' => false,
                'message' => "The file {$file->name} was deleted",
                'data'=> $file_deleted], 200);
        }

        return response()->json(['error' => true, 'message' => 'Does not exists a file with that id'], 404);
    }
}
