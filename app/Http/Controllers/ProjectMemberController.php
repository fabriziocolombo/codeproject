<?php

namespace CodeProject\Http\Controllers;

use Illuminate\Http\Request;

use CodeProject\Services\ProjectService;

class ProjectMemberController extends ApiController
{
    protected $service;

    public function __construct(ProjectService $service)
    {
        $this->service = $service;
        //parent::__construct();

        $this->middleware('check-project-permissions', ['only' => ['index', 'show']]);
        $this->middleware('check-project-owner', ['only' => ['store','update','destroy']]);
    }

    public function index($project_id)
    {
        $data = $this->service->allByProject($project_id);

        return response()->api(['data' => $data ]);
    }

    public function store(Request $request, $project_id)
    {
        return $this->service->addMembership($request->only('user_id'), $project_id);
    }

    public function destroy($project_id, $member_id)
    {
        return $this->service->removeMembership($project_id, $member_id);
    }

    public function show($project_id, $member_id)
    {
        $data = $this->service->isMember($project_id, $member_id);

        return response()->api(['data' => $data,
                                'message' => 'This member is associated with this project' ]);
    }
}
