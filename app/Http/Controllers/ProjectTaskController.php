<?php

namespace CodeProject\Http\Controllers;

use Illuminate\Http\Request;

use CodeProject\Services\ProjectTaskService;

class ProjectTaskController extends ApiController
{
    protected $service;

    public function __construct(ProjectTaskService $service)
    {
        $this->service = $service;
        parent::__construct();

        $this->middleware('check-project-permissions', ['only' => ['index', 'show']]);
        $this->middleware('check-project-owner', ['only' => ['store','update','destroy']]);
    }

    public function index($project_id)
    {
        $data = $this->service->all($project_id);

        return response()->api(['data' => $data]);
    }

    public function show($id, $task_id)
    {
        $data = $this->service->find($id, $task_id);

        return response()->api(['data' => $data]);
    }

    public function store(Request $request)
    {
        $data = $this->service->create($request->all());

        return response()->api(['data' => $data,
                                'message' => 'This Entity has been created' ]);
    }

    public function update(Request $request, $id, $task_id)
    {
        $data = $this->service->update($request->all(), $task_id);

        return response()->api(['data' => $data,
                                'message' => 'This Entity has been updated' ]);
    }

    public function destroy($id, $task_id)
    {
        $data = $this->service->delete($task_id);

        return response()->api(['data' => $data,
                                'message' => 'This Entity has been deleted' ]);
    }
}
