<?php

namespace CodeProject\Http\Requests;

use CodeProject\Http\Requests\Request;

class CreateFileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
        [
            'name' => 'required',
            'description' => 'required',
            'file' => 'required|max:30000'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'É necessário informar um nome',
            'file.required'  => 'É necessário selecionar um Arquivo ',
        ];
    }

    /*
    * Laravel generates a JSON response containing all of the validation errors.
    *
    * This JSON response will besent with a 422 HTTP status code.
    */
    public function response(array $errors)
    {
        return response()->json(['error' => true, 'message' => $errors, 'code' => 422], 422);
    }
}
