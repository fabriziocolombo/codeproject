<?php

namespace CodeProject\Transformers;

use League\Fractal\TransformerAbstract;

use CodeProject\Entities\Client;

class ClientTransformer extends TransformerAbstract
{

    public function transform(Client $model)
    {
        return [
            'id'         => (int) $model->id,
            'name' => $model->name,
            'responsible' => $model->responsible,
            'email' => $model->email,
            'phone' => $model->phone,
            'address' => $model->address,
            'obs' => $model->obs
        ];
    }
}
