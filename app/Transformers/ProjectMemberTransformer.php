<?php

namespace CodeProject\Transformers;

use CodeProject\Entities\User;
use League\Fractal\TransformerAbstract;

class ProjectMemberTransformer extends TransformerAbstract
{
    public function __construct(int $project_id)
    {
        $this->project_id = $project_id;
    }

    public function transform(User $member)
    {
        return [
            'member_id' => $member->id,
            'name' => $member->name,
            'links'   => [
                'rel' => 'self',
                'uri' => '/project/'. $this->project_id.'/memberships/'. $member->id ,
            ]
        ];
    }
}
