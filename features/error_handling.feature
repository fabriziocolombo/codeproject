Feature: Accessing an invalid url
       In order to known about my mistakes
       As an API client
       I should receive an error response
     Scenario:
       When I send a GET request to "/404"
       Then the response code should be 404
