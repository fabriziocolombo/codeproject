Feature: Project API
    In order to validate the send request step
    As a context developer
    I need to be able to send a request with values in a scenario

    Scenario: GET /project
      When I send a GET request to "/project"
      Then response code should be 200
      And the response should contain json:
      """
      {
        "id" : "1"
      }
      """

      Scenario: GET /project/{id}
        When I send a GET request to "/project/1"
        Then response code should be 200
        And the response should contain json:
        """
        {
          "id" : "1",
          "name": "voluptatem"
        }
        """
