<?php

$factory->define(CodeProject\Entities\ProjectNote::class, function (Faker\Generator $faker) {
    return [
      'project_id' => rand(1,10),
      'title' => $faker->word,
      'note' => $faker->sentence
    ];
});
