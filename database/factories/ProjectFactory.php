<?php

$factory->define(CodeProject\Entities\Project::class, function (Faker\Generator $faker) {
    return [
      'owner_id' => rand(1,10),
      'client_id' => rand(1,10),
    	'name' => $faker->word,
    	'description' => $faker->sentence,
    	'progress' => rand(1,100),
    	'status' => rand(1,3),
      'due_date' => $faker->dateTime('now'),
    ];
});
