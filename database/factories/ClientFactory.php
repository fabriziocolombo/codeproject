<?php

$factory->define(CodeProject\Entities\Client::class, function (Faker\Generator $faker) {
    return [
    	'name' => $faker->name,
    	'responsible' => $faker->name,
    	'email' => $faker->email,
    	'phone'=> $faker->phoneNumber,
    	'address' => $faker->address,
    	'obs' => $faker->sentence,
    ];
});
