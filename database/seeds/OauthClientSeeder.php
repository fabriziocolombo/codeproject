<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class OauthClientSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        $faker = Faker::create();
        
		for($i = 0; $i < 5; $i++)
		{
			DB::table('oauth_clients')->insert
			([
				'id' => "appid$i",
				'secret' => "secret$i",
				'name' => "NameApp$i",
                'created_at' => $faker->dateTime('now'),
                'updated_at' => $faker->dateTime('now'),
                //\Carbon\Carbon::now()

			]);
		}
	}
}
