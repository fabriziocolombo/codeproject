angular.module('app.controllers')
    .controller('ProjectNoteListController', [
        '$scope', '$routeParams', 'ProjectNote',
        function($scope, $routeParams, ProjectNote) {

            ProjectNote.query({id: $routeParams.id}).$promise.then(success, error);

            function success(value) {
                $scope.projectNotes = value.data;
                $scope.projectId = $routeParams.id;
            }

            function error(value) {
                toastr.error(value.data.error_description);
            }

        }
    ]);