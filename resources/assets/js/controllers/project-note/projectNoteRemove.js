angular.module('app.controllers')
    .controller('ProjectNoteRemoveController', ['$scope', '$location', '$routeParams', 'ProjectNote',
        function($scope, $location, $routeParams, ProjectNote) {

            $scope.projectNote = ProjectNote.get({
                id: $routeParams.id,
                noteId: $routeParams.noteId
            }).$promise.then(function(response) {
                $scope.projectNote = response.data[0];
            });


            // remove data form
            $scope.remove = function() {
                ProjectNote.remove({
                    id: $routeParams.id,
                    noteId: $routeParams.noteId
                }, $scope.projectNote).$promise.then(success, error);
            }


            function success(response) {
                toastr.success("excluido com sucesso!");
                $location.path('/project/' + $routeParams.id + '/notes');
            }

            function error(response) {
                toastr.error(response.data.message);

            }

        }
    ]);