angular.module('app.controllers')
    .controller('ProjectNoteEditController', ['$scope', '$location', '$routeParams', 'ProjectNote',
        function($scope, $location, $routeParams, ProjectNote) {

            $scope.projectNote = ProjectNote.get({
                id: $routeParams.id,
                noteId: $routeParams.noteId
            }).$promise.then(function(response) {
                $scope.projectNote = response.data[0];
            });


            // save data form
            $scope.save = function() {
                if ($scope.form.$valid) {
                    ProjectNote.update({
                        id: $routeParams.id,
                        noteId: $routeParams.noteId
                    }, $scope.projectNote).$promise.then(success, error);
                };

            }


            function success(response) {
                toastr.success("atualizado com sucesso!");
                $location.path('/project/' + $routeParams.id + '/notes');
            }

            function error(response) {
                toastr.error(response.data.message);

            }


        }
    ]);