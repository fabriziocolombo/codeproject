angular.module('app.controllers')
    .controller('ClientRemoveController', ['$scope', '$location', '$routeParams', 'Client',
        function($scope, $location, $routeParams, Client) {

            $scope.client = Client.get({id: $routeParams.id })
                .$promise.then(function(response) {
                    $scope.client = response.data[0];
                });

            // remove data form
            $scope.remove = function() {
                Client.remove({
                        id: $scope.client.id
                    }, $scope.client).$promise.then(success, error);
            }
            
            
             function success(response) {
                toastr.success("excluido com sucesso!");
                $location.path('/clients');
            }

            function error(response) {
                toastr.error(response.data.message);

            }
        }
    ]);