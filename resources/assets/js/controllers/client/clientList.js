angular.module('app.controllers')
    .controller('ClientListController',
        ['$scope', 'Client', function ($scope, Client) {
            //Client.query(function(response) {
            //    console.log(response.error);
            //    $scope.clients = response.data;
            //});

            //Client.query();

            Client.query().$promise.then(success, error);

            function success(value) {
                $scope.clients = value.data;
            }

            function error(value) {
                toastr.error(value.data.error_description);
            }

        }]);