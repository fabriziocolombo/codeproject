angular.module('app.controllers')
    .controller('ClientEditController', ['$scope', '$location', '$routeParams', 'Client',
        function($scope, $location, $routeParams, Client) {

            var client = Client.get({
                id: $routeParams.id
            }, function(success) {
                $scope.client = success.data[0];
            }, function(error) {
                $location.path('/clients');
                toastr.success(error.data.message);
            });


            // save data form
            $scope.save = function() {

                if ($scope.form.$valid) {
                    Client.update({
                        id: $scope.client.id
                    }, $scope.client).$promise.then(success, error);
                };

            }


            function success(response) {
                toastr.success("atualizado com sucesso!");
                $location.path('/clients');
            }

            function error(response) {
                toastr.error(response.data.message);

            }
        }
    ]);